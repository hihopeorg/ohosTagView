# ohosTagView

本项目是基于开源项目TagView进行ohos化的移植和开发，可以通过项目标签以及github地址（https://github.com/whilu/AndroidTagView ）追踪到原项目版本

#### 项目介绍

- 项目名称：标签控件
- 所属系列：ohos的第三方组件适配移植
- 功能：支持自定义标签样式。
- 项目移植状态：完成
- 调用差异：无
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 原项目Doc地址：https://github.com/whilu/AndroidTagView
- 原项目基线版本：V1.1.7
- 编程语言：Java

#### 演示效果

![Image text](/screenshot/ohosTagView.gif)

#### 安装教程
方法1.
1. 下载har包ohosTagView.har。
2. 启动 DevEco Studio，将下载的har包，导入工程目录“entry->libs”下。
3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。
```
repositories {
    flatDir { dirs 'libs' }
}
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    implementation(name: 'ohosTagView', ext: 'har')
    ....
}
```
方法2.
1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址
```
repositories {
    maven {
        url 'http://106.15.92.248:8081/repository/Releases/'
    }
}
```
2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:
```
dependencies {
    implementation 'co.lujun.ohos:ohosTagView:1.0.1'
}
```

#### 使用说明

1. 在布局文件里使用：
```xml
<co.lujun.ohostagview.TagContainerLayout
    ohos:id="$+id:tagcontainerLayout1"
    ohos:width="match_parent"
    ohos:height="match_content"
    ohos:padding="10vp"
    app:container_enable_drag="true"
    app:horizontal_interval="10vp"
    app:tag_clickable="true"
    app:tag_enable_cross="true"
    app:tag_theme="2"
    app:vertical_interval="10vp" />
```

2. 在代码里这样使用。
```java
TagContainerLayout mTagContainerLayout = (TagContainerLayout) findComponentById(ResourceTable.Id_tagcontainerLayout1);
mTagContainerLayout.setTags(List<String> tags);
```

3. 你也可以设置一些监听：
```java
mTagContainerLayout.setOnTagClickListener(new TagView.OnTagClickListener() {

    @Override
    public void onTagClick(int position, String text) {
        // ...
    }

    @Override
    public void onTagLongClick(final int position, String text) {
        // ...
    }

    @Override
    public void onSelectedTagDrag(int position, String text){
        // ...
    }
    
    @Override
    public void onTagCrossClick(int position) {
        // ...
    }
});
```

下面是一些自定义属性:
|name|format|description|
|:---:|:---:|:---:|
| vertical_interval | dimension | Vertical interval, default 5(vp)|
| horizontal_interval | dimension | Horizontal interval, default 5(vp)|
| container_border_width | dimension | TagContainerLayout border width(default 0.5vp)|
| container_border_radius | dimension | TagContainerLayout border radius(default 10.0vp)|
| container_border_color | color | TagContainerLayout border color(default #22FF0000)|
| container_background_color | color | TagContainerLayout background color(default #11FF0000)|
| container_enable_drag | boolean | Can drag TagView(default false)|
| container_drag_sensitivity | float | The sensitive of the ViewDragHelper(default 1.0f, normal)|
| container_gravity | enum | The TagContainerLayout|
| container_max_lines | integer | The max lines for TagContainerLayout(default 0, auto increase)|
| tag_border_width | dimension | TagView Border width(default 0.5vp)|
| tag_corner_radius | dimension | TagView Border radius(default 15.0vp)|
| tag_horizontal_padding | dimension | Horizontal padding for TagView, include left and right padding(left and right padding are equal, default 10vp)|
| tag_vertical_padding | dimension | Vertical padding for TagView, include top and bottom padding(top and bottom padding are equal, default 8vp)|
| tag_text_size | dimension | TagView Text size(default 14fp)|
| tag_bd_distance | dimension | The distance between baseline and descent(default 2.75vp)|
| tag_text_color | color | TagView text color(default #FF666666)|
| tag_border_color | color | TagView border color(default #88F44336)|
| tag_background_color | color | TagView background color(default #33F44336)|
| tag_max_length | integer | The max length for TagView(default max length 23)|
| tag_clickable | boolean | Whether TagView can clickable(default false)|
| tag_selectable | boolean | Whether TagView can be selectable(default false)|
| tag_theme | enum | The TagView [theme](#themes)|
| tag_text_direction | enum | The TagView text [direction](#directions)|
| tag_ripple_color | color | The ripple effect color(default #EEEEEE)|
| tag_ripple_alpha | integer | The ripple effect color alpha(the value may between 0 - 255, default 128)|
| tag_ripple_duration | integer | The ripple effect duration(In milliseconds, default 1000ms)|
| tag_enable_cross | boolean | Enable draw cross icon(default false)|
| tag_cross_width | dimension | The cross area width(your cross click area, default equal to the TagView's height)|
| tag_cross_color | color | The cross icon color(default Color.BLACK)|
| tag_cross_line_width | dimension | The cross line width(default 1vp)|
| tag_cross_area_padding | dimension | The padding of the cross area(default 10vp)|
| tag_support_letters_rlt | boolean | Whether to support 'letters show with RTL' style(default false)|
| tag_background | reference | TagView background resource(default none background)|

##### <span id="themes">Themes</span>

|theme|code|value|description|
|:---:|:---:|:---:|:---:|
| none | ColorFactory.NONE | -1 | **If you customize TagView with your way, set this theme**|
| random | ColorFactory.RANDOM | 0 | Create each TagView using random color|
| pure_cyan | ColorFactory.PURE_CYAN | 1 | All TagView created by pure cyan color|
| pure_teal | ColorFactory.PURE_TEAL | 2 | All TagView created by pure teal color|

**设置属性之后，可以设置或添加一个tag**

* 使用 ```setTags()``` 设置tag, 需要的参数类型 ```List<String>``` or ```String[]```.
```java
mTagContainerLayout.setTags(List<String> tags);
```
* 在ContainerLayout末尾添加tag.
```java
mTagContainerLayout.addTag(String text);
```
* 在ContainerLayout指定位置插入tag.
```java
mTagContainerLayout.addTag(String text, int position);
```
* 根据position移除一个TagView.
```java
mTagContainerLayout.removeTag(int position);
```
* 移除所有的TagViews.
```java
mTagContainerLayout.removeAllTags();
```
* 获取指定位置的TagView.
```java
mTagContainerLayout.getTagView(int position);
```
* 为每个TagView设置颜色.
```java
List<int[]> colors = new ArrayList<int[]>();
//int[] color = {TagBackgroundColor, TabBorderColor, TagTextColor, TagSelectedBackgroundColor}
int[] color1 = {Color.RED, Color.BLACK, Color.WHITE, Color.YELLOW};
int[] color2 = {Color.BLUE, Color.BLACK, Color.WHITE, Color.YELLOW};
colors.add(color1);
colors.add(color2);
mTagcontainerLayout.setTags(tags, colors);
```

#### 版本迭代

- v1.0.1

#### 版权和许可信息
```
Copyright 2015 lujun

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
