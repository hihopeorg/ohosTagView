package co.lujun.sample;

import co.lujun.ohostagview.TagContainerLayout;
import co.lujun.ohostagview.TagView;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.components.TextField;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.TextAlignment;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.dialog.IDialog;
import ohos.agp.window.dialog.ToastDialog;
import ohos.global.resource.Resource;

import java.util.ArrayList;
import java.util.List;

public class MainAbility extends Ability {

    private TagContainerLayout mTagContainerLayout1, mTagContainerLayout2,
            mTagContainerLayout3, mTagContainerLayout4, mTagcontainerLayout5;
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        mTagContainerLayout1 = (TagContainerLayout) findComponentById(ResourceTable.Id_tagcontainerLayout1);

        List<String> list1 = new ArrayList<String>();
        list1.add("Java");
        list1.add("C++");
        list1.add("Python");
        list1.add("Swift");
        list1.add("你好，这是一个TAG。你好，这是...");
        list1.add("PHP");
        list1.add("JavaScript");
        list1.add("Html");
        list1.add("Welcome to use ohosTagView!");

        mTagContainerLayout1.setDefaultImageDrawableID(ResourceTable.Media_yellow_avatar);

        // Set custom click listener
        mTagContainerLayout1.setOnTagClickListener(new TagView.OnTagClickListener() {
            @Override
            public void onTagClick(int position, String text) {
                showToast("click text: " + text);
            }

            @Override
            public void onTagLongClick(final int position, String text) {
                CommonDialog dialog = new CommonDialog(MainAbility.this);
                dialog.setTitleText("   long click");
                dialog.setContentText("   You will delete this tag!");
                dialog.setSize(900,350);
                dialog.setButton(1, "Delete", new IDialog.ClickedListener() {
                    @Override
                    public void onClick(IDialog iDialog, int i) {
                        if (position < mTagContainerLayout1.getChildCount()) {
                            mTagContainerLayout1.removeTag(position);
                        }
                        dialog.destroy();
                    }
                });
                dialog.setButton(0, "Cancel", new IDialog.ClickedListener() {
                    @Override
                    public void onClick(IDialog iDialog, int i) {
                        dialog.destroy();
                    }
                });
                dialog.show();
            }

            @Override
            public void onSelectedTagDrag(int position, String text) {}

            @Override
            public void onTagCrossClick(int position) {
                //mTagContainerLayout1.removeTag(position);
                showToast("Click TagView cross! position = " + position);
            }
        });

        mTagContainerLayout1.setTags(list1);

        mTagContainerLayout2 = (TagContainerLayout) findComponentById(ResourceTable.Id_tagcontainerLayout2);
        List<String> list2 = new ArrayList<String>();
        list2.add("China");
        list2.add("USA");
        list2.add("Austria");
        list2.add("Japan");
        list2.add("Sudan");
        list2.add("Spain");
        list2.add("UK");
        list2.add("Germany");
        list2.add("Niger");
        list2.add("Poland");
        list2.add("Norway");
        list2.add("Uruguay");
        list2.add("Brazil");
        mTagContainerLayout2.setTags(list2);

        mTagContainerLayout3 = (TagContainerLayout) findComponentById(ResourceTable.Id_tagcontainerLayout3);
        String[] list3 = new String[]{"Persian", "波斯语", "فارسی", "Hello", "你好", "سلام"};
        mTagContainerLayout3.setOnTagClickListener(new TagView.OnTagClickListener() {
            @Override
            public void onTagClick(int position, String text) {
                List<Integer> selectedPositions = mTagContainerLayout3.getSelectedTagViewPositions();
                //deselect all tags when click on an unselected tag. Otherwise show toast.
                if (selectedPositions.isEmpty() || selectedPositions.contains(position)) {
                    showToast("click-position:" + position + ", text:" + text);
                } else {
                    //deselect all tags
                    for (int i : selectedPositions) {
                        mTagContainerLayout3.deselectTagView(i);
                    }
                }

            }

            @Override
            public void onTagLongClick(final int position, String text) {
                mTagContainerLayout3.toggleSelectTagView(position);

                List<Integer> selectedPositions = mTagContainerLayout3.getSelectedTagViewPositions();
                showToast("selected-positions:" + selectedPositions.toString());
            }

            @Override
            public void onSelectedTagDrag(int position, String text) {
//                ClipData clip = ClipData.newPlainText("Text", text);
//                View view = mTagContainerLayout3.getTagView(position);
//                View.DragShadowBuilder shadow = new View.DragShadowBuilder(view);
//                view.startDrag(clip, shadow, Boolean.TRUE, 0);
            }

            @Override
            public void onTagCrossClick(int position) {
            }
        });
        mTagContainerLayout3.setTags(list3);

        mTagContainerLayout4 = (TagContainerLayout) findComponentById(ResourceTable.Id_tagcontainerLayout4);
        String[] list4 = new String[]{"Adele", "Whitney Houston"};
        mTagContainerLayout4.setTags(list4);

        mTagcontainerLayout5 = (TagContainerLayout) findComponentById(ResourceTable.Id_tagcontainerLayout5);
        List<String> list5 = new ArrayList<String>();
        list5.add("Custom Red Color");
        list5.add("Custom Blue Color");
        List<int[]> colors = new ArrayList<int[]>();
        //int[]color = {backgroundColor, tagBorderColor, tagTextColor, tagSelectedBackgroundColor}
        int[] col1 = {Color.getIntColor("#ff0000"), Color.getIntColor("#000000"), Color.getIntColor("#ffffff"), Color.getIntColor("#999999")};
        int[] col2 = {Color.getIntColor("#0000ff"), Color.getIntColor("#000000"), Color.getIntColor("#ffffff"), Color.getIntColor("#999999")};

        colors.add(col1);
        colors.add(col2);
        mTagcontainerLayout5.setTags(list5, colors);

        TextField textField = (TextField) findComponentById(ResourceTable.Id_text_tag);
        Button button = (Button) findComponentById(ResourceTable.Id_btn_add_tag);
        button.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                mTagContainerLayout1.addTag(textField.getText());
            }
        });
    }

    private void showToast(String txt) {
        Text text = new Text(this);
        text.setText(txt);
        text.setTextSize(18, Text.TextSizeType.FP);
        text.setTextAlignment(TextAlignment.CENTER);
        text.setMultipleLine(true);
        ToastDialog toastDialog= new ToastDialog(this);
        toastDialog.setComponent(text).setAlignment(LayoutAlignment.CENTER).setDuration(1000).show();
    }

}
