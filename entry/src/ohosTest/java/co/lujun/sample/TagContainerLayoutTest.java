package co.lujun.sample;

import co.lujun.ohostagview.TagContainerLayout;
import co.lujun.ohostagview.TagView;
import co.lujun.ohostagview.Utils;
import co.lujun.ohostagview.ViewDragHelper;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.Component;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.app.Context;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class TagContainerLayoutTest {

    private TagContainerLayout tagContainerLayout;

    @Before
    public void setUp() throws Exception {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        tagContainerLayout = new TagContainerLayout(context);
        List<String> list2 = new ArrayList<String>();
        list2.add("China");
        list2.add("USA");
        list2.add("Austria");
        list2.add("Japan");
        list2.add("Sudan");
        list2.add("Spain");
        list2.add("UK");
        list2.add("Germany");
        list2.add("Niger");
        list2.add("Poland");
        list2.add("Norway");
        list2.add("Uruguay");
        list2.add("Brazil");
        tagContainerLayout.setTags(list2);

    }

    @Test
    public void getTagViewState() throws NoSuchFieldException, IllegalAccessException {
        Field field = tagContainerLayout.getClass().getDeclaredField("mTagViewState");
        field.setAccessible(true);
        field.set(tagContainerLayout, ViewDragHelper.STATE_IDLE);
        assertEquals(ViewDragHelper.STATE_IDLE, tagContainerLayout.getTagViewState());
    }

    @Test
    public void getTagBdDistance() {
        tagContainerLayout.setTagBdDistance(10f);
        assertEquals(Utils.dp2px(tagContainerLayout.getContext(), 10f), tagContainerLayout.getTagBdDistance(), 0);
    }

    @Test
    public void addTag() {
        tagContainerLayout.addTag("test");
        assertEquals(14, tagContainerLayout.size());
    }

    @Test
    public void removeTag() {
        tagContainerLayout.removeTag(12);
        assertEquals(12, tagContainerLayout.size());
    }

    @Test
    public void removeConsecutiveTags() {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        tagContainerLayout.removeConsecutiveTags(list);
        assertEquals(11, tagContainerLayout.size());
    }

    @Test
    public void removeAllTags() {
        tagContainerLayout.removeAllTags();
        assertEquals(0, tagContainerLayout.size());
    }

    @Test
    public void setOnTagClickListener() throws NoSuchFieldException, IllegalAccessException {
        tagContainerLayout.setOnTagClickListener(new TagView.OnTagClickListener() {
            @Override
            public void onTagClick(int position, String text) {
            }

            @Override
            public void onTagLongClick(int position, String text) {
            }

            @Override
            public void onSelectedTagDrag(int position, String text) {
            }

            @Override
            public void onTagCrossClick(int position) {
            }
        });
        Field field = tagContainerLayout.getClass().getDeclaredField("mOnTagClickListener");
        field.setAccessible(true);
        assertNotNull(field.get(tagContainerLayout));
    }

    @Test
    public void getTagText() {
        assertEquals("China", tagContainerLayout.getTagText(0));
    }

    @Test
    public void getDragEnable() {
        tagContainerLayout.setDragEnable(true);
        assertEquals(true, tagContainerLayout.getDragEnable());
    }

    @Test
    public void getVerticalInterval() {
        tagContainerLayout.setVerticalInterval(10f);
        assertEquals(Utils.dp2px(tagContainerLayout.getContext(), 10f), tagContainerLayout.getVerticalInterval(), 0.5);
    }

    @Test
    public void getHorizontalInterval() {
        tagContainerLayout.setHorizontalInterval(10f);
        assertEquals(Utils.dp2px(tagContainerLayout.getContext(), 10f), tagContainerLayout.getHorizontalInterval(), 0.5);
    }

    @Test
    public void setBorderWidth() {
        tagContainerLayout.setBorderWidth(10f);
        assertEquals(10f, tagContainerLayout.getBorderWidth(), 0);
    }

    @Test
    public void getBorderRadius() {
        tagContainerLayout.setBorderRadius(10f);
        assertEquals(10f, tagContainerLayout.getBorderRadius(), 0);
    }

    @Test
    public void getBorderColor() {
        tagContainerLayout.setBorderColor(Color.RED.getValue());
        assertEquals(Color.RED.getValue(), tagContainerLayout.getBorderColor());
    }

    @Test
    public void getBackgroundColor() {
        tagContainerLayout.setBackgroundColor(Color.RED.getValue());
        assertEquals(Color.RED.getValue(), tagContainerLayout.getBackgroundColor());
    }

    @Test
    public void getGravity() {
        tagContainerLayout.setGravity(LayoutAlignment.CENTER);
        assertEquals(LayoutAlignment.CENTER, tagContainerLayout.getGravity());
    }

    @Test
    public void getSensitivity() {
        tagContainerLayout.setSensitivity(10f);
        assertEquals(10f, tagContainerLayout.getSensitivity(), 0);
    }

    @Test
    public void getDefaultImageDrawableID() {
        tagContainerLayout.setDefaultImageDrawableID(ResourceTable.Media_yellow_avatar);
        assertEquals(ResourceTable.Media_yellow_avatar, tagContainerLayout.getDefaultImageDrawableID());
    }

    @Test
    public void setMaxLines() {
        tagContainerLayout.setMaxLines(2);
        assertEquals(2, tagContainerLayout.getMaxLines());
    }

    @Test
    public void getTagMaxLength() {
        tagContainerLayout.setTagMaxLength(4);
        assertEquals(4, tagContainerLayout.getTagMaxLength());
    }

    @Test
    public void getIsTagViewClickable() {
        tagContainerLayout.setIsTagViewClickable(true);
        assertEquals(true, tagContainerLayout.getIsTagViewClickable());
    }

    @Test
    public void getIsTagViewSelectable() {
        tagContainerLayout.setIsTagViewSelectable(true);
        assertEquals(true, tagContainerLayout.getIsTagViewSelectable());
    }

    @Test
    public void getTagBorderWidth() {
        tagContainerLayout.setTagBorderWidth(10f);
        assertEquals(10f, tagContainerLayout.getTagBorderWidth(), 0);
    }

    @Test
    public void getTagBorderRadius() {
        tagContainerLayout.setTagBorderRadius(10f);
        assertEquals(10f, tagContainerLayout.getTagBorderRadius(), 0);
    }

    @Test
    public void getTagTextSize() {
        tagContainerLayout.setTagTextSize(10);
        assertEquals(10, tagContainerLayout.getTagTextSize(), 0);
    }

    @Test
    public void getTagHorizontalPadding() {
        tagContainerLayout.setTagHorizontalPadding(10);
        assertEquals(10, tagContainerLayout.getTagHorizontalPadding());
    }

    @Test
    public void getTagVerticalPadding() {
        tagContainerLayout.setTagVerticalPadding(10);
        assertEquals(10, tagContainerLayout.getTagVerticalPadding());
    }

    @Test
    public void getTagBorderColor() {
        tagContainerLayout.setTagBorderColor(Color.RED.getValue());
        assertEquals(Color.RED.getValue(), tagContainerLayout.getTagBorderColor());
    }

    @Test
    public void getTagBackgroundColor() {
        tagContainerLayout.setTagBackgroundColor(Color.RED.getValue());
        assertEquals(Color.RED.getValue(), tagContainerLayout.getTagBackgroundColor());
    }

    @Test
    public void getTagTextColor() {
        tagContainerLayout.setTagTextColor(Color.RED.getValue());
        assertEquals(Color.RED.getValue(), tagContainerLayout.getTagTextColor());
    }

    @Test
    public void getTagTypeface() {
        tagContainerLayout.setTagTypeface(Font.DEFAULT_BOLD);
        assertEquals(Font.DEFAULT_BOLD, tagContainerLayout.getTagTypeface());
    }

    @Test
    public void getTagTextDirection() {
        tagContainerLayout.setTagTextDirection(Component.LayoutDirection.LTR);
        assertEquals(Component.LayoutDirection.LTR, tagContainerLayout.getTagTextDirection());
    }

    @Test
    public void getRippleAlpha() {
        tagContainerLayout.setRippleAlpha(1);
        assertEquals(1, tagContainerLayout.getRippleAlpha());
    }

    @Test
    public void getRippleColor() {
        tagContainerLayout.setRippleColor(Color.BLUE.getValue());
        assertEquals(Color.BLUE.getValue(), tagContainerLayout.getRippleColor());
    }

    @Test
    public void getRippleDuration() {
        tagContainerLayout.setRippleDuration(300);
        assertEquals(300, tagContainerLayout.getRippleDuration());
    }

    @Test
    public void getCrossColor() {
        tagContainerLayout.setCrossColor(Color.BLUE.getValue());
        assertEquals(Color.BLUE.getValue(), tagContainerLayout.getCrossColor());
    }

    @Test
    public void getCrossAreaPadding() {
        tagContainerLayout.setCrossAreaPadding(10f);
        assertEquals(10f, tagContainerLayout.getCrossAreaPadding(), 0);
    }

    @Test
    public void isEnableCross() {
        tagContainerLayout.setEnableCross(true);
        assertEquals(true, tagContainerLayout.isEnableCross());
    }

    @Test
    public void getCrossAreaWidth() {
        tagContainerLayout.setCrossAreaWidth(10f);
        assertEquals(10f, tagContainerLayout.getCrossAreaWidth(), 0);
    }

    @Test
    public void getCrossLineWidth() {
        tagContainerLayout.setCrossLineWidth(10f);
        assertEquals(10f, tagContainerLayout.getCrossLineWidth(), 0);
    }

    @Test
    public void isTagSupportLettersRTL() {
        tagContainerLayout.setTagSupportLettersRTL(true);
        assertEquals(true, tagContainerLayout.isTagSupportLettersRTL());
    }

    @Test
    public void getTagBackgroundResource() {
        tagContainerLayout.setTagBackgroundResource(ResourceTable.Graphic_background_ability_main);
        assertEquals(ResourceTable.Graphic_background_ability_main, tagContainerLayout.getTagBackgroundResource());
    }
}