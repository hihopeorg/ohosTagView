package co.lujun.sample.UITest;

import co.lujun.ohostagview.ColorFactory;
import co.lujun.ohostagview.TagContainerLayout;
import co.lujun.ohostagview.TagView;
import co.lujun.sample.MainAbility;
import co.lujun.sample.ResourceTable;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.Button;
import ohos.agp.components.TextField;
import ohos.agp.utils.Color;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.io.IOException;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MainAbilityTest {
    private static Ability ability = EventHelper.startAbility(MainAbility.class);
    static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x01214, "ohosTagTest");

    private void stopThread(int x) {
        try {
            Thread.sleep(x);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void exec(String x) {
        try {
            Runtime.getRuntime().exec(x);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test01View() {
        TagContainerLayout tagContainerLayout1 = (TagContainerLayout) ability.findComponentById(ResourceTable.Id_tagcontainerLayout1);
        Assert.assertTrue("第一类标签未正常显示", tagContainerLayout1 != null && tagContainerLayout1.getChildCount() != 0);
        Assert.assertTrue("第一类标签样式显示失败", tagContainerLayout1.getTheme() == ColorFactory.PURE_TEAL);
        int defaultImageDrawableID = tagContainerLayout1.getDefaultImageDrawableID();
        int resId = ResourceTable.Media_yellow_avatar;
        HiLog.warn(LABEL, defaultImageDrawableID + ":" + resId);
        Assert.assertTrue("第一类标签图标显示失败", defaultImageDrawableID == resId);


        TagContainerLayout tagContainerLayout2 = (TagContainerLayout) ability.findComponentById(ResourceTable.Id_tagcontainerLayout2);
        Assert.assertTrue("第二类标签未正常显示", tagContainerLayout2 != null && tagContainerLayout2.getChildCount() != 0);
        Assert.assertTrue("第二类标签样式显示失败", tagContainerLayout2.getTheme() == ColorFactory.RANDOM);

        TagContainerLayout tagContainerLayout3 = (TagContainerLayout) ability.findComponentById(ResourceTable.Id_tagcontainerLayout3);
        Assert.assertTrue("第三类标签未正常显示", tagContainerLayout3 != null && tagContainerLayout3.getChildCount() != 0);
        Assert.assertTrue("第三类标签样式显示失败", tagContainerLayout3.getTheme() == ColorFactory.NONE);

        TagContainerLayout tagContainerLayout4 = (TagContainerLayout) ability.findComponentById(ResourceTable.Id_tagcontainerLayout4);
        Assert.assertTrue("第四类标签未正常显示", tagContainerLayout4 != null && tagContainerLayout4.getChildCount() != 0);
        Assert.assertTrue("第四类标签样式显示失败", tagContainerLayout4.getTheme() == ColorFactory.PURE_TEAL);

        TagContainerLayout tagContainerLayout5 = (TagContainerLayout) ability.findComponentById(ResourceTable.Id_tagcontainerLayout5);
        Assert.assertTrue("第五类标签未正常显示", tagContainerLayout5 != null && tagContainerLayout5.getChildCount() != 0);
        Assert.assertTrue("第五类标签样式显示失败", tagContainerLayout5.getTheme() == ColorFactory.RANDOM);
        TagView tagView = tagContainerLayout5.getTagView(0);
        TagView tagView1 = tagContainerLayout5.getTagView(1);
        Assert.assertTrue("第五类标签显示异常", tagView.getTagBackgroundColor() == Color.getIntColor("#ff0000") && tagView1.getTagBackgroundColor() == Color.getIntColor("#0000ff"));

        stopThread(3000);
    }

    @Test
    public void test02Add() {
        TagContainerLayout tagContainerLayout1 = (TagContainerLayout) ability.findComponentById(ResourceTable.Id_tagcontainerLayout1);
        stopThread(2000);
        TextField textField = (TextField) ability.findComponentById(ResourceTable.Id_text_tag);
        Button button = (Button) ability.findComponentById(ResourceTable.Id_btn_add_tag);
        int childCount = tagContainerLayout1.getChildCount();
        ability.getUITaskDispatcher().asyncDispatch(() -> {
            textField.setText("add new tag");
        });
        stopThread(2000);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability, button);
        int childCount1 = tagContainerLayout1.getChildCount();
        Assert.assertTrue("增加失败", childCount + 1 == childCount1);
        stopThread(2000);


    }

}
