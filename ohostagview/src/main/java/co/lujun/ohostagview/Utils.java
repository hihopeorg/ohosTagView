/*
 * Copyright 2015 lujun
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package co.lujun.ohostagview;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.utils.Color;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

import java.util.NoSuchElementException;

/**
 * Author: lujun(http://blog.lujun.co)
 * Date: 2016-12-7 21:53
 */

public class Utils {

    public static float dp2px(Context context, float dp) {
        final float scale = DisplayManager.getInstance().getDefaultDisplay(context).get().getAttributes().scalDensity;
        return dp * scale + 0.5f;
    }

    public static float sp2px(Context context, float sp) {
        final float scale = DisplayManager.getInstance().getDefaultDisplay(context).get().getAttributes().scalDensity;
        return sp * scale;
    }

    /**
     * If the color is Dark, make it lighter and vice versa
     *
     * @param color in int,
     * @param factor The factor greater than 0.0 and smaller than 1.0
     * @return int
     */
    public static int manipulateColorBrightness(int color, float factor) {
        int a = Color.alpha(color);
        RgbColor rgbColor = new RgbColor(color);
        int r = rgbColor.getRed();
        int g = rgbColor.getGreen();
        int b = rgbColor.getBlue();
//        if (r + b + g < 128 * 3) factor = 1 / factor;// check if the color is bright or dark
//        r = Math.round(r * factor);
//        b = Math.round(b * factor);
//        g = Math.round(g * factor);
        if (r > 127) r = 255 - Math.round((255 - r) * factor);
        if (g > 127) g = 255 - Math.round((255 - g) * factor);
        if (b > 127) b = 255 - Math.round((255 - b) * factor);

        return Color.argb(a,
                Math.min(r, 255),
                Math.min(g, 255),
                Math.min(b, 255)
        );
    }

    public static int getDimensionValue(AttrSet attrSet, String name, int defaultValue) {
        if (attrSet == null) {
            return defaultValue;
        }
        try {
            int value = attrSet.getAttr(name).get().getDimensionValue();
            return value;
        } catch (NoSuchElementException e) {
            return defaultValue;
        }
    }

    public static Color getColorValue(AttrSet attrSet, String name, Color defaultColor) {
        if (attrSet == null) {
            return defaultColor;
        }
        try {
            Color color = attrSet.getAttr(name).get().getColorValue();
            return color;
        } catch (NoSuchElementException e) {
            return defaultColor;
        }
    }

    public static boolean getBoolValue(AttrSet attrSet, String name, boolean defaultValue) {
        if (attrSet == null) {
            return defaultValue;
        }
        try {
            boolean value = attrSet.getAttr(name).get().getBoolValue();
            return value;
        } catch (NoSuchElementException e) {
            return defaultValue;
        }
    }

    public static float getFloatValue(AttrSet attrSet, String name, float defaultValue) {
        if (attrSet == null) {
            return defaultValue;
        }
        try {
            float value = attrSet.getAttr(name).get().getFloatValue();
            return value;
        } catch (NoSuchElementException e) {
            return defaultValue;
        }
    }

    public static int getIntegerValue(AttrSet attrSet, String name, int defaultValue) {
        if (attrSet == null) {
            return defaultValue;
        }
        try {
            int value = attrSet.getAttr(name).get().getIntegerValue();
            return value;
        } catch (NoSuchElementException e) {
            return defaultValue;
        }
    }

    public static Component.LayoutDirection getDirectionValue(AttrSet attrSet, String name, Component.LayoutDirection defaultValue) {
        if (attrSet == null) {
            return defaultValue;
        }
        try {
            String value = attrSet.getAttr(name).get().getStringValue();
            if (value.equals("rtl")) {
                return Component.LayoutDirection.RTL;
            }
            return defaultValue;
        } catch (NoSuchElementException e) {
            return defaultValue;
        }
    }
}
