/*
 * Copyright 2015 lujun
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package co.lujun.ohostagview;

import ohos.agp.components.AttrHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.text.Font;
import ohos.agp.utils.*;
import ohos.app.Context;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static co.lujun.ohostagview.Utils.dp2px;
import static co.lujun.ohostagview.Utils.sp2px;

/**
 * Author: lujun(http://blog.lujun.co)
 * Date: 2015-12-30 17:14
 */
public class TagContainerLayout extends ComponentContainer implements Component.DrawTask, Component.TouchEventListener {

    /**
     * Vertical interval, default 5(dp)
     */
    private int mVerticalInterval;

    /**
     * The list to store the tags color info
     */
    private List<int[]> mColorArrayList;

    /**
     * Horizontal interval, default 5(dp)
     */
    private int mHorizontalInterval;

    /**
     * TagContainerLayout border width(default 0.5dp)
     */
    private float mBorderWidth = 0.5f;

    /**
     * TagContainerLayout border radius(default 10.0dp)
     */
    private float mBorderRadius = 10.0f;

    /**
     * The sensitive of the ViewDragHelper(default 1.0f, normal)
     */
    private float mSensitivity = 1.0f;

    /**
     * TagView average height
     */
    private int mChildHeight;

    /**
     * TagContainerLayout border color(default #22FF0000)
     */
    private int mBorderColor = Color.getIntColor("#22FF0000");

    /**
     * TagContainerLayout background color(default #11FF0000)
     */
    private int mBackgroundColor = Color.getIntColor("#11FF0000");

    /**
     * The container layout gravity(default left)
     */
    private int mGravity = LayoutAlignment.LEFT;

    /**
     * The max line count of TagContainerLayout
     */
    private int mMaxLines = 0;

    /**
     * The max length for TagView(default max length 23)
     */
    private int mTagMaxLength = 23;

    /**
     * TagView Border width(default 0.5dp)
     */
    private float mTagBorderWidth = 0.5f;

    /**
     * TagView Border radius(default 15.0dp)
     */
    private float mTagBorderRadius = 15.0f;

    /**
     * TagView Text size(default 14sp)
     */
    private int mTagTextSize = 14;

    /**
     * Text direction(support:TEXT_DIRECTION_RTL & TEXT_DIRECTION_LTR, default TEXT_DIRECTION_LTR)
     */
    private LayoutDirection mTagTextDirection = LayoutDirection.LTR;

    /**
     * Horizontal padding for TagView, include left & right padding(left & right padding are equal, default 10dp)
     */
    private int mTagHorizontalPadding = 10;

    /**
     * Vertical padding for TagView, include top & bottom padding(top & bottom padding are equal, default 8dp)
     */
    private int mTagVerticalPadding = 8;

    /**
     * TagView border color(default #88F44336)
     */
    private int mTagBorderColor = Color.getIntColor("#88F44336");

    /**
     * TagView background color(default #33F44336)
     */
    private int mTagBackgroundColor = Color.getIntColor("#33F44336");

    /**
     * Selected TagView background color(default #33FF7669)
     */
    private int mSelectedTagBackgroundColor = Color.getIntColor("#33FF7669");

    /**
     * TagView text color(default #FF666666)
     */
    private int mTagTextColor = Color.getIntColor("#FF666666");

    /**
     * TagView typeface
     */
    private Font mTagTypeface = Font.DEFAULT;

    /**
     * Whether TagView can clickable(default unclickable)
     */
    private boolean isTagViewClickable;

    /**
     * Whether TagView can selectable(default unselectable)
     */
    private boolean isTagViewSelectable;

    /**
     * Tags
     */
    private List<String> mTags;

    /**
     * Default image for new tags
     */
    private int mDefaultImageDrawableID = -1;

    /**
     * Can drag TagView(default false)
     */
    private boolean mDragEnable;

    /**
     * The distance between baseline and descent(default 2.75dp)
     */
    private float mTagBdDistance = 2.75f;

    /**
     * OnTagClickListener for TagView
     */
    private TagView.OnTagClickListener mOnTagClickListener;

    /**
     * Whether to support 'letters show with RTL' style(default false)
     */
    private boolean mTagSupportLettersRTL = false;

    private Paint mPaint;

    private RectFloat mRectF;

    private List<Component> mChildViews;

    private int[] mViewPos;

    /**
     * View theme(default PURE_CYAN)
     */
    private int mTheme = ColorFactory.PURE_CYAN;

    /**
     * Default interval(dp)
     */
    private static final float DEFAULT_INTERVAL = 5;

    /**
     * Default tag min length
     */
    private static final int TAG_MIN_LENGTH = 3;

    /**
     * The ripple effect duration(In milliseconds, default 1000ms)
     */
    private int mRippleDuration = 1000;

    /**
     * The ripple effect color(default #EEEEEE)
     */
    private int mRippleColor;

    /**
     * The ripple effect color alpha(the value may between 0 - 255, default 128)
     */
    private int mRippleAlpha = 128;

    /**
     * Enable draw cross icon(default false)
     */
    private boolean mEnableCross = false;

    /**
     * The cross area width(your cross click area, default equal to the TagView's height)
     */
    private float mCrossAreaWidth = 0.0f;

    /**
     * The padding of the cross area(default 10dp)
     */
    private float mCrossAreaPadding = 10.0f;

    /**
     * The cross icon color(default Color.BLACK)
     */
    private int mCrossColor = Color.BLACK.getValue();

    /**
     * The cross line width(default 1dp)
     */
    private float mCrossLineWidth = 1.0f;

    /**
     * TagView background resource
     */
    private int mTagBackgroundResource;

    private int draggedIndex = -1, lastX = -1, lastY = -1, lastTargetIndex = -1;

    public TagContainerLayout(Context context) {
        this(context, null);
    }

    public TagContainerLayout(Context context, AttrSet attrs) {
        this(context, attrs, null);
    }

    public TagContainerLayout(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    private void init(Context context, AttrSet attrs, String defStyleAttr) {

        mVerticalInterval = Utils.getDimensionValue(attrs,"vertical_interval", (int) AttrHelper.vp2px(DEFAULT_INTERVAL, context));
        mHorizontalInterval =  Utils.getDimensionValue(attrs,"horizontal_interval", (int) AttrHelper.vp2px(DEFAULT_INTERVAL, context));
        mBorderWidth = Utils.getDimensionValue(attrs,"container_border_width", (int) AttrHelper.vp2px(mBorderWidth, context));
        mBorderRadius = Utils.getDimensionValue(attrs,"container_border_radius", (int) AttrHelper.vp2px(mBorderRadius, context));
        mTagBdDistance = Utils.getDimensionValue(attrs,"tag_bd_distance", (int) AttrHelper.vp2px(mTagBdDistance, context));
        mBorderColor = Utils.getColorValue(attrs, "container_border_color", new Color(mBorderColor)).getValue();
        mBackgroundColor = Utils.getColorValue(attrs, "container_background_color", new Color(mBackgroundColor)).getValue();
        mDragEnable = Utils.getBoolValue(attrs,"container_enable_drag", false);
        mSensitivity = Utils.getFloatValue(attrs, "container_drag_sensitivity", mSensitivity);
        mGravity = Utils.getIntegerValue(attrs,"container_gravity", mGravity);
        mMaxLines = Utils.getIntegerValue(attrs,"container_max_lines", mMaxLines);
        mTagMaxLength = Utils.getIntegerValue(attrs,"tag_max_length", mTagMaxLength);
        mTheme = Utils.getIntegerValue(attrs,"tag_theme", mTheme);
        mTagBorderWidth = Utils.getDimensionValue(attrs,"tag_border_width", (int) AttrHelper.vp2px(mTagBorderWidth, context));
        mTagBorderRadius = Utils.getDimensionValue(attrs,"tag_corner_radius", (int) AttrHelper.vp2px(mTagBorderRadius, context));
        mTagHorizontalPadding = Utils.getDimensionValue(attrs,"tag_horizontal_padding", (int) AttrHelper.vp2px(mTagHorizontalPadding, context));
        mTagVerticalPadding = Utils.getDimensionValue(attrs,"tag_vertical_padding", (int) AttrHelper.vp2px(mTagVerticalPadding, context));
        mTagTextSize = Utils.getDimensionValue(attrs,"tag_text_size", (int) AttrHelper.fp2px(mTagTextSize, context));
        mTagBorderColor = Utils.getColorValue(attrs, "tag_border_color", new Color(mTagBorderColor)).getValue();
        mTagBackgroundColor = Utils.getColorValue(attrs, "tag_background_color", new Color(mTagBackgroundColor)).getValue();
        mTagTextColor = Utils.getColorValue(attrs, "tag_text_color", new Color(mTagTextColor)).getValue();
        mTagTextDirection = Utils.getDirectionValue(attrs,"tag_text_direction", mTagTextDirection);
        isTagViewClickable = Utils.getBoolValue(attrs,"tag_clickable", false);
        isTagViewSelectable = Utils.getBoolValue(attrs,"tag_selectable", false);
        mRippleColor = Utils.getColorValue(attrs, "tag_ripple_color", new Color(Color.getIntColor("#EEEEEE"))).getValue();
        mRippleAlpha = Utils.getIntegerValue(attrs,"tag_ripple_alpha", mRippleAlpha);
        mRippleDuration = Utils.getIntegerValue(attrs,"tag_ripple_duration", mRippleDuration);
        mEnableCross = Utils.getBoolValue(attrs,"tag_enable_cross", mEnableCross);
        mCrossAreaWidth = Utils.getDimensionValue(attrs,"tag_cross_width", (int) AttrHelper.vp2px(mCrossAreaWidth, context));
        mCrossAreaPadding = Utils.getDimensionValue(attrs,"tag_cross_area_padding", (int) AttrHelper.vp2px(mCrossAreaPadding, context));
        mCrossColor = Utils.getColorValue(attrs, "tag_cross_color", new Color(mCrossColor)).getValue();
        mCrossLineWidth = Utils.getDimensionValue(attrs,"tag_cross_line_width", (int) AttrHelper.vp2px(mCrossLineWidth, context));
        mTagSupportLettersRTL = Utils.getBoolValue(attrs,"tag_support_letters_rlt", mTagSupportLettersRTL);

        mPaint = new Paint();
        mRectF = new RectFloat();
        mChildViews = new ArrayList<Component>();
        setTagMaxLength(mTagMaxLength);
        setTagHorizontalPadding(mTagHorizontalPadding);
        setTagVerticalPadding(mTagVerticalPadding);

        setLayoutRefreshedListener(refreshedListener);
        setEstimateSizeListener(estimateSizeListener);
        setArrangeListener(arrangeListener);
        addDrawTask(this::onDraw);
        setTouchEventListener(this::onTouchEvent);

    }

    EstimateSizeListener estimateSizeListener = new EstimateSizeListener() {
        @Override
        public boolean onEstimateSize(int widthMeasureSpec, int heightMeasureSpec) {
            final int childCount = getChildCount();
            for(int i = 0; i < childCount; i++) {
                Component child = getComponentAt(i);
                child.estimateSize(widthMeasureSpec, heightMeasureSpec);
            }

            int widthSpecSize = EstimateSpec.getSize(widthMeasureSpec);
            int widthSpecMode = MeasureSpec.getMode(widthMeasureSpec);
            int heightSpecSize = EstimateSpec.getSize(heightMeasureSpec);
            int heightSpecMode = EstimateSpec.getMode(heightMeasureSpec);
            setEstimatedSize(widthSpecSize + getPaddingLeft() + getPaddingRight(), heightSpecSize);
            int lines = childCount == 0 ? 0 : getChildLines(childCount);
            if (childCount == 0) {
                setEstimatedSize(0, 0);
            } else if (heightSpecMode == EstimateSpec.NOT_EXCEED
                    || heightSpecMode == EstimateSpec.UNCONSTRAINT) {
                setEstimatedSize(widthSpecSize + getPaddingLeft() + getPaddingRight(),(mVerticalInterval + mChildHeight) * lines
                        - mVerticalInterval + getPaddingTop() + getPaddingBottom());
            } else {
                setEstimatedSize(widthSpecSize, heightSpecSize);
            }
            return true;
        }
    };

    LayoutRefreshedListener refreshedListener = new LayoutRefreshedListener() {
        @Override
        public void onRefreshed(Component component) {
            mRectF.modify(0, 0, component.getWidth(), component.getHeight());
        }
    };

    ArrangeListener arrangeListener = new ArrangeListener() {
        @Override
        public boolean onArrange(int l, int t, int w, int h) {
            int childCount;
            if ((childCount = getChildCount()) <= 0) {
                return false;
            }
            int availableW = getEstimatedWidth() - getPaddingLeft() - getPaddingRight();
            int curRight = getEstimatedWidth() - getPaddingRight();
            int curTop = 0;//getPaddingTop();
            int curLeft = 0;//getPaddingLeft();
            int sPos = 0;
            mViewPos = new int[childCount * 2];

            for (int i = 0; i < childCount; i++) {
                final Component childView = getComponentAt(i);
                if (childView.getVisibility() != HIDE) {
                    int width = childView.getEstimatedWidth();
                    if (mGravity == LayoutAlignment.RIGHT) {
                        if (curRight - width < getPaddingLeft()) {
                            curRight = getEstimatedWidth() - getPaddingRight();
                            curTop += mChildHeight + mVerticalInterval;
                        }
                        mViewPos[i * 2] = curRight - width;
                        mViewPos[i * 2 + 1] = curTop;
                        curRight -= width + mHorizontalInterval;
                    } else if (mGravity == LayoutAlignment.CENTER) {
                        if (curLeft + width - getPaddingLeft() > availableW) {
                            int leftW = getEstimatedWidth() - mViewPos[(i - 1) * 2]
                                    - getComponentAt(i - 1).getEstimatedWidth() - getPaddingRight();
                            for (int j = sPos; j < i; j++) {
                                mViewPos[j * 2] = mViewPos[j * 2] + leftW / 2;
                            }
                            sPos = i;
                            curLeft = getPaddingLeft();
                            curTop += mChildHeight + mVerticalInterval;
                        }
                        mViewPos[i * 2] = curLeft;
                        mViewPos[i * 2 + 1] = curTop;
                        curLeft += width + mHorizontalInterval;

                        if (i == childCount - 1) {
                            int leftW = getEstimatedWidth() - mViewPos[i * 2]
                                    - childView.getEstimatedWidth() - getPaddingRight();
                            for (int j = sPos; j < childCount; j++) {
                                mViewPos[j * 2] = mViewPos[j * 2] + leftW / 2;
                            }
                        }
                    } else {
                        if (curLeft + width - getPaddingLeft() > availableW) {
                            curLeft = 0;//getPaddingLeft();
                            curTop += mChildHeight + mVerticalInterval;
                        }
                        mViewPos[i * 2] = curLeft;
                        mViewPos[i * 2 + 1] = curTop;
                        curLeft += width + mHorizontalInterval;
                    }
                }
            }

            // layout all child views
            for (int i = 0; i < mViewPos.length / 2; i++) {
                Component childView = getComponentAt(i);
                childView.arrange(mViewPos[i * 2], mViewPos[i * 2 + 1],
                        childView.getEstimatedWidth(), mChildHeight);
            }
            return true;
        }
    };

    @Override
    public void onDraw(Component component, Canvas canvas) {
        mRectF.modify(0, 0, component.getWidth(), component.getHeight());
        mPaint.setStyle(Paint.Style.FILL_STYLE);
        mPaint.setColor(new Color(mBackgroundColor));
        canvas.drawRoundRect(mRectF, mBorderRadius, mBorderRadius, mPaint);

        mPaint.setStyle(Paint.Style.STROKE_STYLE);
        mPaint.setStrokeWidth(mBorderWidth);
        mPaint.setColor(new Color(mBorderColor));
        canvas.drawRoundRect(mRectF, mBorderRadius, mBorderRadius, mPaint);
    }

//    @Override
//    public boolean onTouchEvent(Component component, TouchEvent event) {
//        mViewDragHelper.processTouchEvent(event);
//        return true;
//    }

    @Override
    public boolean onTouchEvent(Component view, TouchEvent event) {
        if (!mDragEnable) {
            return false;
        }
        int action = event.getAction();
        int [] location = view.getLocationOnScreen();
        MmiPoint point = event.getPointerScreenPosition(event.getIndex());
        float x = point.getX() - location[0];
        float y = point.getY() - location[1];
        switch (action) {
            case TouchEvent.PRIMARY_POINT_DOWN:
                lastX = (int)x;
                lastY = (int)y;
                draggedIndex = getIndex((int)x, (int)y);
                break;
            case TouchEvent.POINT_MOVE:
                int deltaX = (int)x - lastX;
                int deltaY = (int)y - lastY;
                if (draggedIndex != -1) {
                    Component childView = getComponentAt(draggedIndex);
                    if (childView.getTag() != null && childView.getTag().equals("undraggable")) {
                        return false;
                    }
                    int itemLeft = childView.getLeft(), itemTop = childView.getTop();
                    childView.arrange(itemLeft + deltaX, itemTop + deltaY, childView.getEstimatedWidth(), mChildHeight);
                    //得到当前点击位置所在的item的index
                    int targetIndex = getIndex((int)x, (int)y);
                    if (lastTargetIndex != targetIndex && targetIndex != -1) {
                        lastTargetIndex = targetIndex;
                    }
                }
                lastX = (int)x;
                lastY = (int)y;
                break;
            case TouchEvent.PRIMARY_POINT_UP:
                if (draggedIndex != -1) {
                    //如果存在item交换 则重新排列子view
                    if (lastTargetIndex != -1) {
                        Component child = getComponentAt(draggedIndex);
                        int [] pos = onGetNewPosition(child);
                        int posRefer = onGetCoordinateReferPos(pos[0], pos[1]);
                        onChangeView(child, posRefer, draggedIndex);
                        child.invalidate();
                    }
                    lastTargetIndex = -1;
                    draggedIndex = -1;
                }
                break;
        }
        //如果存在拖动item 并且需要消费掉该事件 则返回true
        if (draggedIndex != -1) {
            return true;
        }
        return true;
    }

    private int getChildLines(int childCount) {
        int availableW = getEstimatedWidth() - getPaddingLeft() - getPaddingRight();
        int lines = 1;
        for (int i = 0, curLineW = 0; i < childCount; i++) {
            Component childView = getComponentAt(i);
            int dis = childView.getEstimatedWidth() + mHorizontalInterval;
            int height = childView.getEstimatedHeight();
            mChildHeight = i == 0 ? height : Math.min(mChildHeight, height);
            curLineW += dis;
            if (curLineW - mHorizontalInterval > availableW) {
                lines++;
                curLineW = dis;
            }
        }

        return mMaxLines <= 0 ? lines : mMaxLines;
    }

    private int[] onUpdateColorFactory() {
        int[] colors;
        if (mTheme == ColorFactory.RANDOM) {
            colors = ColorFactory.onRandomBuild();
        } else if (mTheme == ColorFactory.PURE_TEAL) {
            colors = ColorFactory.onPureBuild(ColorFactory.PURE_COLOR.TEAL);
        } else if (mTheme == ColorFactory.PURE_CYAN) {
            colors = ColorFactory.onPureBuild(ColorFactory.PURE_COLOR.CYAN);
        } else {
            colors = new int[]{mTagBackgroundColor, mTagBorderColor, mTagTextColor, mSelectedTagBackgroundColor};
        }
        return colors;
    }

    private void onSetTag() {
        if (mTags == null) {
            throw new RuntimeException("NullPointer exception!");
        }
        removeAllTags();
        if (mTags.size() == 0) {
            return;
        }
        for (int i = 0; i < mTags.size(); i++) {
            onAddTag(mTags.get(i), mChildViews.size());
        }
        invalidate();
    }

    private void onAddTag(String text, int position) {
        if (position < 0 || position > mChildViews.size()) {
            throw new RuntimeException("Illegal position!");
        }
        TagView tagView;
        if (mDefaultImageDrawableID != -1) {
            tagView = new TagView(getContext(), text, mDefaultImageDrawableID);
        } else {
            tagView = new TagView(getContext(), text);
        }
        initTagView(tagView, position);
        mChildViews.add(position, tagView);
        if (position < mChildViews.size()) {
            for (int i = position; i < mChildViews.size(); i++) {
                mChildViews.get(i).setTag(i);
            }
        } else {
            tagView.setTag(position);
        }
        addComponent(tagView, position);
    }

    private void initTagView(TagView tagView, int position) {
        int[] colors;
        if (mColorArrayList != null && mColorArrayList.size() > 0) {
            if (mColorArrayList.size() == mTags.size() &&
                    mColorArrayList.get(position).length >= 4) {
                colors = mColorArrayList.get(position);
            } else {
                throw new RuntimeException("Illegal color list!");
            }
        } else {
            colors = onUpdateColorFactory();
        }

        tagView.setTagBackgroundColor(colors[0]);
        tagView.setTagBorderColor(colors[1]);
        tagView.setTagTextColor(colors[2]);
        tagView.setTagSelectedBackgroundColor(colors[3]);
        tagView.setTagMaxLength(mTagMaxLength);
        tagView.setTextDirection(mTagTextDirection);
        tagView.setTypeface(mTagTypeface);
        tagView.setBorderWidth(mTagBorderWidth);
        tagView.setBorderRadius(mTagBorderRadius);
        tagView.setTextSize(mTagTextSize);
        tagView.setHorizontalPadding(mTagHorizontalPadding);
        tagView.setVerticalPadding(mTagVerticalPadding);
        tagView.setIsViewClickable(isTagViewClickable);
        tagView.setIsViewSelectable(isTagViewSelectable);
        tagView.setBdDistance(mTagBdDistance);
        tagView.setOnTagClickListener(mOnTagClickListener);
        tagView.setRippleAlpha(mRippleAlpha);
        tagView.setRippleColor(mRippleColor);
        tagView.setRippleDuration(mRippleDuration);
        tagView.setEnableCross(mEnableCross);
        tagView.setCrossAreaWidth(mCrossAreaWidth);
        tagView.setCrossAreaPadding(mCrossAreaPadding);
        tagView.setCrossColor(mCrossColor);
        tagView.setCrossLineWidth(mCrossLineWidth);
        tagView.setTagSupportLettersRTL(mTagSupportLettersRTL);
        //tagView.setBackgroundResource(mTagBackgroundResource);
    }

    private void invalidateTags() {
        for (Component view : mChildViews) {
            final TagView tagView = (TagView) view;
            tagView.setOnTagClickListener(mOnTagClickListener);
        }
    }

    private void onRemoveTag(int position) {
        if (position < 0 || position >= mChildViews.size()) {
            throw new RuntimeException("Illegal position!");
        }
        mChildViews.remove(position);
        removeComponentAt(position);
        for (int i = position; i < mChildViews.size(); i++) {
            mChildViews.get(i).setTag(i);
        }
        // TODO, make removed view null?
    }

    private void onRemoveConsecutiveTags(List<Integer> positions) {
        int smallestPosition = Collections.min(positions);
        for (int position : positions) {
            if (position < 0 || position >= mChildViews.size()) {
                throw new RuntimeException("Illegal position!");
            }
            mChildViews.remove(smallestPosition);
            removeComponentAt(smallestPosition);
        }
        for (int i = smallestPosition; i < mChildViews.size(); i++) {
            mChildViews.get(i).setTag(i);
        }
        // TODO, make removed view null?
    }

    private int[] onGetNewPosition(Component view) {
        int left = view.getLeft();
        int top = view.getTop();
        int bestMatchLeft = mViewPos[(int) view.getTag() * 2];
        int bestMatchTop = mViewPos[(int) view.getTag() * 2 + 1];
        int tmpTopDis = Math.abs(top - bestMatchTop);
        for (int i = 0; i < mViewPos.length / 2; i++) {
            if (Math.abs(top - mViewPos[i * 2 + 1]) < tmpTopDis) {
                bestMatchTop = mViewPos[i * 2 + 1];
                tmpTopDis = Math.abs(top - mViewPos[i * 2 + 1]);
            }
        }
        int rowChildCount = 0;
        int tmpLeftDis = 0;
        for (int i = 0; i < mViewPos.length / 2; i++) {
            if (mViewPos[i * 2 + 1] == bestMatchTop) {
                if (rowChildCount == 0) {
                    bestMatchLeft = mViewPos[i * 2];
                    tmpLeftDis = Math.abs(left - bestMatchLeft);
                } else {
                    if (Math.abs(left - mViewPos[i * 2]) < tmpLeftDis) {
                        bestMatchLeft = mViewPos[i * 2];
                        tmpLeftDis = Math.abs(left - bestMatchLeft);
                    }
                }
                rowChildCount++;
            }
        }
        return new int[]{bestMatchLeft, bestMatchTop};
    }

    private int onGetCoordinateReferPos(int left, int top) {
        int pos = 0;
        for (int i = 0; i < mViewPos.length / 2; i++) {
            if (left == mViewPos[i * 2] && top == mViewPos[i * 2 + 1]) {
                pos = i;
            }
        }
        return pos;
    }

    private int getIndex(int left, int top) {
        for (int i = 0; i < mViewPos.length / 2; i++) {
            Rect rect = new Rect(mViewPos[i * 2], mViewPos[i * 2 + 1],
                    mViewPos[i * 2] + getComponentAt(i).getEstimatedWidth(), mViewPos[i * 2 + 1] + mChildHeight);
            if (rect.isInclude(left, top)) {
                return i;
            }
        }
        return -1;
    }

    private void onChangeView(Component view, int newPos, int originPos) {
        mChildViews.remove(originPos);
        mChildViews.add(newPos, view);
        for (Component child : mChildViews) {
            child.setTag(mChildViews.indexOf(child));
        }

        removeComponentAt(originPos);
        addComponent(view, newPos);
    }

    private int ceilTagBorderWidth() {
        return (int) Math.ceil(mTagBorderWidth);
    }

    /**
     * Get TagView text baseline and descent distance.
     *
     * @return
     */
    public float getTagBdDistance() {
        return mTagBdDistance;
    }

    /**
     * Set TagView text baseline and descent distance.
     *
     * @param tagBdDistance
     */
    public void setTagBdDistance(float tagBdDistance) {
        this.mTagBdDistance = dp2px(getContext(), tagBdDistance);
    }

    /**
     * Set tags
     *
     * @param tags
     */
    public void setTags(List<String> tags) {
        mTags = tags;
        onSetTag();
    }

    /**
     * Set tags with own color
     *
     * @param tags
     * @param colorArrayList
     */
    public void setTags(List<String> tags, List<int[]> colorArrayList) {
        mTags = tags;
        mColorArrayList = colorArrayList;
        onSetTag();
    }

    /**
     * Set tags
     *
     * @param tags
     */
    public void setTags(String... tags) {
        mTags = Arrays.asList(tags);
        onSetTag();
    }

    /**
     * Inserts the specified TagView into this ContainerLayout at the end.
     *
     * @param text
     */
    public void addTag(String text) {
        addTag(text, mChildViews.size());
    }

    /**
     * Inserts the specified TagView into this ContainerLayout at the specified location.
     * The TagView is inserted before the current element at the specified location.
     *
     * @param text
     * @param position
     */
    public void addTag(String text, int position) {
        onAddTag(text, position);
        invalidate();
    }

    /**
     * Remove a TagView in specified position.
     *
     * @param position
     */
    public void removeTag(int position) {
        onRemoveTag(position);
        invalidate();
    }

    /**
     * Remove TagView in multiple consecutive positions.
     *
     *
     */
    public void removeConsecutiveTags(List<Integer> positions) {
        onRemoveConsecutiveTags(positions);
        invalidate();
    }

    /**
     * Remove all TagViews.
     */
    public void removeAllTags() {
        mChildViews.clear();
        removeAllComponents();
        invalidate();
    }

    /**
     * Set OnTagClickListener for TagView.
     *
     * @param listener
     */
    public void setOnTagClickListener(TagView.OnTagClickListener listener) {
        mOnTagClickListener = listener;
        invalidateTags();
    }

    /**
     * Toggle select a tag
     *
     * @param position
     */
    public void toggleSelectTagView(int position) {
        if (isTagViewSelectable){
            TagView tagView = ((TagView)mChildViews.get(position));
            if (tagView.getIsViewSelected()){
                tagView.deselectView();
            } else {
                tagView.selectView();
            }
        }
    }

    /**
     * Select a tag
     *
     * @param position
     */
    public void selectTagView(int position) {
        if (isTagViewSelectable)
            ((TagView)mChildViews.get(position)).selectView();
    }

    /**
     * Deselect a tag
     *
     * @param position
     */
    public void deselectTagView(int position) {
        if (isTagViewSelectable)
            ((TagView)mChildViews.get(position)).deselectView();
    }

    /**
     * Return selected TagView positions
     *
     * @return list of selected positions
     */
    public List<Integer> getSelectedTagViewPositions() {
        List<Integer> selectedPositions = new ArrayList<>();
        for (int i = 0; i < mChildViews.size(); i++){
            if (((TagView)mChildViews.get(i)).getIsViewSelected()){
                selectedPositions.add(i);
            }
        }
        return selectedPositions;
    }

    /**
     * Return selected TagView text
     *
     * @return list of selected tag text
     */
    public List<String> getSelectedTagViewText() {
        List<String> selectedTagText = new ArrayList<>();
        for (int i = 0; i < mChildViews.size(); i++){
            TagView tagView = (TagView)mChildViews.get(i);
            if ((tagView.getIsViewSelected())){
                selectedTagText.add(tagView.getText());
            }
        }
        return selectedTagText;
    }

    /**
     * Return number of child tags
     *
     * @return size
     */
    public int size() {
        return mChildViews.size();
    }

    /**
     * Get TagView text.
     *
     * @param position
     * @return
     */
    public String getTagText(int position) {
        return ((TagView) mChildViews.get(position)).getText();
    }

    /**
     * Get a string list for all tags in TagContainerLayout.
     *
     * @return
     */
    public List<String> getTags() {
        List<String> tmpList = new ArrayList<String>();
        for (Component view : mChildViews) {
            if (view instanceof TagView) {
                tmpList.add(((TagView) view).getText());
            }
        }
        return tmpList;
    }

    /**
     * Set whether the child view can be dragged.
     *
     * @param enable
     */
    public void setDragEnable(boolean enable) {
        this.mDragEnable = enable;
    }

    /**
     * Get current view is drag enable attribute.
     *
     * @return
     */
    public boolean getDragEnable() {
        return mDragEnable;
    }

    /**
     * Set vertical interval
     *
     * @param interval
     */
    public void setVerticalInterval(float interval) {
        mVerticalInterval = (int) dp2px(getContext(), interval);
        invalidate();
    }

    /**
     * Get vertical interval in this view.
     *
     * @return
     */
    public int getVerticalInterval() {
        return mVerticalInterval;
    }

    /**
     * Set horizontal interval.
     *
     * @param interval
     */
    public void setHorizontalInterval(float interval) {
        mHorizontalInterval = (int) dp2px(getContext(), interval);
        invalidate();
    }

    /**
     * Get horizontal interval in this view.
     *
     * @return
     */
    public int getHorizontalInterval() {
        return mHorizontalInterval;
    }

    /**
     * Get TagContainerLayout border width.
     *
     * @return
     */
    public float getBorderWidth() {
        return mBorderWidth;
    }

    /**
     * Set TagContainerLayout border width.
     *
     * @param width
     */
    public void setBorderWidth(float width) {
        this.mBorderWidth = width;
    }

    /**
     * Get TagContainerLayout border radius.
     *
     * @return
     */
    public float getBorderRadius() {
        return mBorderRadius;
    }

    /**
     * Set TagContainerLayout border radius.
     *
     * @param radius
     */
    public void setBorderRadius(float radius) {
        this.mBorderRadius = radius;
    }

    /**
     * Get TagContainerLayout border color.
     *
     * @return
     */
    public int getBorderColor() {
        return mBorderColor;
    }

    /**
     * Set TagContainerLayout border color.
     *
     * @param color
     */
    public void setBorderColor(int color) {
        this.mBorderColor = color;
    }

    /**
     * Get TagContainerLayout background color.
     *
     * @return
     */
    public int getBackgroundColor() {
        return mBackgroundColor;
    }

    /**
     * Set TagContainerLayout background color.
     *
     * @param color
     */
    public void setBackgroundColor(int color) {
        this.mBackgroundColor = color;
    }

    /**
     * Get container layout gravity.
     *
     * @return
     */
    public int getGravity() {
        return mGravity;
    }

    /**
     * Set container layout gravity.
     *
     * @param gravity
     */
    public void setGravity(int gravity) {
        this.mGravity = gravity;
    }

    /**
     * Get TagContainerLayout ViewDragHelper sensitivity.
     *
     * @return
     */
    public float getSensitivity() {
        return mSensitivity;
    }

    /**
     * Set TagContainerLayout ViewDragHelper sensitivity.
     *
     * @param sensitivity
     */
    public void setSensitivity(float sensitivity) {
        this.mSensitivity = sensitivity;
    }

    /**
     * Get default tag image
     *
     * @return
     */
    public int getDefaultImageDrawableID() {
        return mDefaultImageDrawableID;
    }

    /**
     * Set default image for tags.
     *
     * @param imageID
     */
    public void setDefaultImageDrawableID(int imageID) {
        this.mDefaultImageDrawableID = imageID;
    }

    /**
     * Set max line count for TagContainerLayout
     *
     * @param maxLines max line count
     */
    public void setMaxLines(int maxLines) {
        mMaxLines = maxLines;
        invalidate();
    }

    /**
     * Get TagContainerLayout's max lines
     *
     * @return maxLines
     */
    public int getMaxLines() {
        return mMaxLines;
    }

    /**
     * Set the TagView text max length(must greater or equal to 3).
     *
     * @param maxLength
     */
    public void setTagMaxLength(int maxLength) {
        mTagMaxLength = maxLength < TAG_MIN_LENGTH ? TAG_MIN_LENGTH : maxLength;
    }

    /**
     * Get TagView max length.
     *
     * @return
     */
    public int getTagMaxLength() {
        return mTagMaxLength;
    }

    /**
     * Set TagView theme.
     *
     * @param theme
     */
    public void setTheme(int theme) {
        mTheme = theme;
    }

    /**
     * Get TagView theme.
     *
     * @return
     */
    public int getTheme() {
        return mTheme;
    }

    /**
     * Get TagView is clickable.
     *
     * @return
     */
    public boolean getIsTagViewClickable() {
        return isTagViewClickable;
    }

    /**
     * Set TagView is clickable
     *
     * @param clickable
     */
    public void setIsTagViewClickable(boolean clickable) {
        this.isTagViewClickable = clickable;
    }

    /**
     * Get TagView is selectable.
     *
     * @return
     */
    public boolean getIsTagViewSelectable() {
        return isTagViewSelectable;
    }

    /**
     * Set TagView is selectable
     *
     * @param selectable
     */
    public void setIsTagViewSelectable(boolean selectable) {
        this.isTagViewSelectable= selectable;
    }

    /**
     * Get TagView border width.
     *
     * @return
     */
    public float getTagBorderWidth() {
        return mTagBorderWidth;
    }

    /**
     * Set TagView border width.
     *
     * @param width
     */
    public void setTagBorderWidth(float width) {
        this.mTagBorderWidth = width;
    }

    /**
     * Get TagView border radius.
     *
     * @return
     */
    public float getTagBorderRadius() {
        return mTagBorderRadius;
    }

    /**
     * Set TagView border radius.
     *
     * @param radius
     */
    public void setTagBorderRadius(float radius) {
        this.mTagBorderRadius = radius;
    }

    /**
     * Get TagView text size.
     *
     * @return
     */
    public float getTagTextSize() {
        return mTagTextSize;
    }

    /**
     * Set TagView text size.
     *
     * @param size
     */
    public void setTagTextSize(int size) {
        this.mTagTextSize = size;
    }

    /**
     * Get TagView horizontal padding.
     *
     * @return
     */
    public int getTagHorizontalPadding() {
        return mTagHorizontalPadding;
    }

    /**
     * Set TagView horizontal padding.
     *
     * @param padding
     */
    public void setTagHorizontalPadding(int padding) {
        int ceilWidth = ceilTagBorderWidth();
        this.mTagHorizontalPadding = padding < ceilWidth ? ceilWidth : padding;
    }

    /**
     * Get TagView vertical padding.
     *
     * @return
     */
    public int getTagVerticalPadding() {
        return mTagVerticalPadding;
    }

    /**
     * Set TagView vertical padding.
     *
     * @param padding
     */
    public void setTagVerticalPadding(int padding) {
        int ceilWidth = ceilTagBorderWidth();
        this.mTagVerticalPadding = padding < ceilWidth ? ceilWidth : padding;
    }

    /**
     * Get TagView border color.
     *
     * @return
     */
    public int getTagBorderColor() {
        return mTagBorderColor;
    }

    /**
     * Set TagView border color.
     *
     * @param color
     */
    public void setTagBorderColor(int color) {
        this.mTagBorderColor = color;
    }

    /**
     * Get TagView background color.
     *
     * @return
     */
    public int getTagBackgroundColor() {
        return mTagBackgroundColor;
    }

    /**
     * Set TagView background color.
     *
     * @param color
     */
    public void setTagBackgroundColor(int color) {
        this.mTagBackgroundColor = color;
    }

    /**
     * Get TagView text color.
     *
     * @return
     */
    public int getTagTextColor() {
        return mTagTextColor;
    }

    /**
     * Set tag text direction, support:View.TEXT_DIRECTION_RTL and View.TEXT_DIRECTION_LTR,
     * default View.TEXT_DIRECTION_LTR
     *
     * @param textDirection
     */
    public void setTagTextDirection(LayoutDirection textDirection) {
        this.mTagTextDirection = textDirection;
    }

    /**
     * Get TagView typeface.
     *
     * @return
     */
    public Font getTagTypeface() {
        return mTagTypeface;
    }

    /**
     * Set TagView typeface.
     *
     * @param typeface
     */
    public void setTagTypeface(Font typeface) {
        this.mTagTypeface = typeface;
    }

    /**
     * Get tag text direction
     *
     * @return
     */
    public LayoutDirection getTagTextDirection() {
        return mTagTextDirection;
    }

    /**
     * Set TagView text color.
     *
     * @param color
     */
    public void setTagTextColor(int color) {
        this.mTagTextColor = color;
    }

    /**
     * Get the ripple effect color's alpha.
     *
     * @return
     */
    public int getRippleAlpha() {
        return mRippleAlpha;
    }

    /**
     * Set TagView ripple effect alpha, the value may between 0 to 255, default is 128.
     *
     * @param mRippleAlpha
     */
    public void setRippleAlpha(int mRippleAlpha) {
        this.mRippleAlpha = mRippleAlpha;
    }

    /**
     * Get the ripple effect color.
     *
     * @return
     */
    public int getRippleColor() {
        return mRippleColor;
    }

    /**
     * Set TagView ripple effect color.
     *
     * @param mRippleColor
     */
    public void setRippleColor(int mRippleColor) {
        this.mRippleColor = mRippleColor;
    }

    /**
     * Get the ripple effect duration.
     *
     * @return
     */
    public int getRippleDuration() {
        return mRippleDuration;
    }

    /**
     * Set TagView ripple effect duration, default is 1000ms.
     *
     * @param mRippleDuration
     */
    public void setRippleDuration(int mRippleDuration) {
        this.mRippleDuration = mRippleDuration;
    }

    /**
     * Set TagView cross color.
     *
     * @return
     */
    public int getCrossColor() {
        return mCrossColor;
    }

    /**
     * Set TagView cross color, default Color.BLACK.
     *
     * @param mCrossColor
     */
    public void setCrossColor(int mCrossColor) {
        this.mCrossColor = mCrossColor;
    }

    /**
     * Get agView cross area's padding.
     *
     * @return
     */
    public float getCrossAreaPadding() {
        return mCrossAreaPadding;
    }

    /**
     * Set TagView cross area padding, default 10dp.
     *
     * @param mCrossAreaPadding
     */
    public void setCrossAreaPadding(float mCrossAreaPadding) {
        this.mCrossAreaPadding = mCrossAreaPadding;
    }

    /**
     * Get is the TagView's cross enable, default false.
     *
     * @return
     */
    public boolean isEnableCross() {
        return mEnableCross;
    }

    /**
     * Enable or disable the TagView's cross.
     *
     * @param mEnableCross
     */
    public void setEnableCross(boolean mEnableCross) {
        this.mEnableCross = mEnableCross;
    }

    /**
     * Get TagView cross area width.
     *
     * @return
     */
    public float getCrossAreaWidth() {
        return mCrossAreaWidth;
    }

    /**
     * Set TagView area width.
     *
     * @param mCrossAreaWidth
     */
    public void setCrossAreaWidth(float mCrossAreaWidth) {
        this.mCrossAreaWidth = mCrossAreaWidth;
    }

    /**
     * Get TagView cross line width.
     *
     * @return
     */
    public float getCrossLineWidth() {
        return mCrossLineWidth;
    }

    /**
     * Set TagView cross line width, default 1dp.
     *
     * @param mCrossLineWidth
     */
    public void setCrossLineWidth(float mCrossLineWidth) {
        this.mCrossLineWidth = mCrossLineWidth;
    }

    /**
     * Get the 'letters show with RTL' style if it's enabled
     *
     * @return
     */
    public boolean isTagSupportLettersRTL() {
        return mTagSupportLettersRTL;
    }

    /**
     * Set whether the 'support letters show with RTL' style is enabled.
     *
     * @param mTagSupportLettersRTL
     */
    public void setTagSupportLettersRTL(boolean mTagSupportLettersRTL) {
        this.mTagSupportLettersRTL = mTagSupportLettersRTL;
    }

    /**
     * Get TagView in specified position.
     *
     * @param position the position of the TagView
     * @return
     */
    public TagView getTagView(int position){
        if (position < 0 || position >= mChildViews.size()) {
            throw new RuntimeException("Illegal position!");
        }
        return (TagView) mChildViews.get(position);
    }

    /**
     * Get TagView background resource
     * @return
     */
    public int getTagBackgroundResource() {
        return mTagBackgroundResource;
    }

    /**
     * Set TagView background resource
     * @param tagBackgroundResource
     */
    public void setTagBackgroundResource(int tagBackgroundResource) {
        this.mTagBackgroundResource = tagBackgroundResource;
    }
}
