/*
 * Copyright 2015 lujun
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package co.lujun.ohostagview;

import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;
import ohos.agp.render.*;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.agp.utils.TextTool;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.global.resource.NotExistException;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.Size;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

import java.io.IOException;

import static co.lujun.ohostagview.Utils.dp2px;

/**
 * Author: lujun(http://blog.lujun.co)
 * Date: 2015-12-31 11:47
 */
public class TagView extends Component implements Component.DrawTask, Component.TouchEventListener {

    /** Border width*/
    private float mBorderWidth;

    /** Border radius*/
    private float mBorderRadius;

    /** Text size*/
    private int mTextSize;

    /** Horizontal padding for this view, include left & right padding(left & right padding are equal*/
    private int mHorizontalPadding;

    /** Vertical padding for this view, include top & bottom padding(top & bottom padding are equal)*/
    private int mVerticalPadding;

    /** TagView border color*/
    private int mBorderColor;

    /** TagView background color*/
    private int mBackgroundColor;

    /** TagView background color*/
    private int mSelectedBackgroundColor;

    /** TagView text color*/
    private int mTextColor;

    /** Whether this view clickable*/
    private boolean isViewClickable;

    /** Whether this view selectable*/
    private boolean isViewSelectable;

    /** Whether this view selected*/
    private boolean isViewSelected;

    /** The max length for this tag view*/
    private int mTagMaxLength;

    /** OnTagClickListener for click action*/
    private OnTagClickListener mOnTagClickListener;

    /** Move slop(default 5dp)*/
    private int mMoveSlop = 5;

    /** Scroll slop threshold 4dp*/
    private int mSlopThreshold = 4;

    /** How long trigger long click callback(default 500ms)*/
    private int mLongPressTime = 500;

    /** Text direction(support:TEXT_DIRECTION_RTL & TEXT_DIRECTION_LTR, default TEXT_DIRECTION_LTR)*/
    private LayoutDirection mTextDirection = LayoutDirection.LTR;

    /** The distance between baseline and descent*/
    private float bdDistance;

    /** Whether to support 'letters show with RTL' style(default false)*/
    private boolean mTagSupportLettersRTL = false;

    private Paint mPaint, mRipplePaint;

    private RectFloat mRectF;

    private String mAbstractText, mOriginText;

    private boolean isUp, isMoved, isExecLongClick;

    private int mLastX, mLastY;

    private float fontH, fontW;

    private float mTouchX, mTouchY;

    /** The ripple effect duration(default 1000ms)*/
    private int mRippleDuration = 1000;

    private float mRippleRadius;

    private int mRippleColor;

    private int mRippleAlpha;

    private Path mPath;

    private Font mTypeface;

    private AnimatorValue mRippleValueAnimator;

    private PixelMap mBitmapImage;

    private boolean mEnableCross;

    private float mCrossAreaWidth;

    private float mCrossAreaPadding;

    private int mCrossColor;

    private float mCrossLineWidth;

    private boolean unSupportedClipPath = false;

    private Runnable mLongClickHandle = new Runnable() {
        @Override
        public void run() {
            if (!isMoved && !isUp){
                isExecLongClick = true;
                mOnTagClickListener.onTagLongClick((int) getTag(), getText());
            }
        }
    };

    public TagView(Context context, String text){
        super(context);
        init(context, text);
    }

    public TagView(Context context, String text, int defaultImageID){
        super(context);
        init(context, text);
        try {
            mBitmapImage = ImageSource.create(getContext().getResourceManager().getResource(defaultImageID),null).createPixelmap(null);
        } catch (IOException | NotExistException e) {
        }
    }

    private void init(Context context, String text){
        mPaint = new Paint();
        mRipplePaint = new Paint();
        mRipplePaint.setStyle(Paint.Style.FILL_STYLE);
        mRectF = new RectFloat();
        mPath = new Path();
        mOriginText = text == null ? "" : text;
        mMoveSlop = (int) dp2px(context, mMoveSlop);
        mSlopThreshold = (int) dp2px(context, mSlopThreshold);

        setLayoutRefreshedListener(refreshedListener);
        setEstimateSizeListener(estimateSizeListener);
        addDrawTask(this::onDraw,DrawTask.BETWEEN_CONTENT_AND_FOREGROUND);
        setTouchEventListener(this::onTouchEvent);

    }

    private void onDealText(){
        if(!TextTool.isNullOrEmpty(mOriginText)) {
            mAbstractText = mOriginText.length() <= mTagMaxLength ? mOriginText
                    : mOriginText.substring(0, mTagMaxLength - 3) + "...";
        }else {
            mAbstractText = "";
        }
        mPaint.setFont(mTypeface);
        mPaint.setTextSize(mTextSize);
        final Paint.FontMetrics fontMetrics = mPaint.getFontMetrics();
        fontH = fontMetrics.descent - fontMetrics.ascent;
        if (mTextDirection == LayoutDirection.RTL){
            fontW = 0;
            for (char c : mAbstractText.toCharArray()) {
                String sc = String.valueOf(c);
                fontW += mPaint.measureText(sc);
            }
        }else {
            fontW = mPaint.measureText(mAbstractText);
        }
    }

    EstimateSizeListener estimateSizeListener = new EstimateSizeListener() {
        @Override
        public boolean onEstimateSize(int widthMeasureSpec, int heightMeasureSpec) {
            int height = mVerticalPadding * 2 + (int) fontH;
            int width = mHorizontalPadding * 2 + (int) fontW + (isEnableCross() ? height : 0) + (isEnableImage() ? height : 0);
            mCrossAreaWidth = Math.min(Math.max(mCrossAreaWidth, height), width);
            setEstimatedSize(EstimateSpec.getSizeWithMode(width,EstimateSpec.PRECISE), EstimateSpec.getSizeWithMode(height,EstimateSpec.PRECISE));
            return true;
        }
    };

    LayoutRefreshedListener refreshedListener = new LayoutRefreshedListener() {
        @Override
        public void onRefreshed(Component component) {
            mRectF.modify(mBorderWidth, mBorderWidth, component.getWidth() - mBorderWidth, component.getHeight() - mBorderWidth);
        }
    };

    @Override
    public void onDraw(Component component, Canvas canvas) {
        // draw background
        mRectF.modify(mBorderWidth, mBorderWidth, component.getWidth() - mBorderWidth, component.getHeight() - mBorderWidth);
        mPaint.setStyle(Paint.Style.FILL_STYLE);
        mPaint.setColor(new Color(getIsViewSelected() ? mSelectedBackgroundColor : mBackgroundColor));
        canvas.drawRoundRect(mRectF, mBorderRadius, mBorderRadius, mPaint);

        // draw border
        mPaint.setStyle(Paint.Style.STROKE_STYLE);
        mPaint.setStrokeWidth(mBorderWidth);
        mPaint.setColor(new Color(mBorderColor));
        canvas.drawRoundRect(mRectF, mBorderRadius, mBorderRadius, mPaint);

        // draw ripple for TagView
        //drawRipple(canvas);

        // draw text
        mPaint.setStyle(Paint.Style.FILL_STYLE);
        mPaint.setColor(new Color(mTextColor));

        if (mTextDirection == LayoutDirection.RTL) {
            if (mTagSupportLettersRTL){
                float tmpX = (isEnableCross() ? getWidth() + getHeight() : getWidth()) / 2
                        + fontW / 2;
                for (char c : mAbstractText.toCharArray()) {
                    String sc = String.valueOf(c);
                    tmpX -= mPaint.measureText(sc);
                    canvas.drawText(mPaint, sc, tmpX, getHeight() / 2 + fontH / 2 - bdDistance);
                }
            }else {
                canvas.drawText(mPaint, mAbstractText,
                        (isEnableCross() ? getWidth() + fontW : getWidth()) / 2 - fontW / 2,
                        getHeight() / 2 + fontH / 2 - bdDistance);
            }
        } else {
            canvas.drawText(mPaint, mAbstractText,
                    (isEnableCross() ? getWidth() - getHeight() : getWidth()) / 2 - fontW / 2 + (isEnableImage() ? getHeight() / 2 : 0),
                    getHeight() / 2 + fontH / 2 - bdDistance);
        }

        // draw cross
        drawCross(canvas);

        // draw image
        drawImage(canvas);
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent event) {
        int action = event.getAction();
        int [] location = component.getLocationOnScreen();
        MmiPoint point = event.getPointerScreenPosition(event.getIndex());
        float x = point.getX() - location[0];
        float y = point.getY() - location[1];
        if (action == TouchEvent.PRIMARY_POINT_DOWN) {
            mRippleRadius = 0.0f;
            mTouchX = x;
            mTouchY = y;
            splashRipple();
        }
        if (isEnableCross() && isClickCrossArea(x) && mOnTagClickListener != null){
            if (action == TouchEvent.PRIMARY_POINT_UP) {
                mOnTagClickListener.onTagCrossClick((int) getTag());
            }
            return true;
        }else if (isViewClickable && mOnTagClickListener != null){
            switch (action){
                case TouchEvent.PRIMARY_POINT_DOWN:
                    mLastY = (int)y;
                    mLastX = (int)x;
                    isMoved = false;
                    isUp = false;
                    isExecLongClick = false;
                    //postDelayed(mLongClickHandle, mLongPressTime);
                    new EventHandler(EventRunner.getMainEventRunner()).postTask(mLongClickHandle, mLongPressTime);
                    break;

                case TouchEvent.POINT_MOVE:
                    if (isMoved){
                        break;
                    }
                    if (Math.abs(mLastX - x) > mMoveSlop || Math.abs(mLastY - y) > mMoveSlop){
                        isMoved = true;
                        if (isViewSelected){
                            mOnTagClickListener.onSelectedTagDrag((int) getTag(), getText());
                        }
                    }
                    break;

                case TouchEvent.PRIMARY_POINT_UP:
                    isUp = true;
                    if (!isExecLongClick && !isMoved) {
                        mOnTagClickListener.onTagClick((int) getTag(), getText());
                    }
                    break;
            }
            return true;
        }
        return false;
    }

    private boolean isClickCrossArea(float x){
        if (mTextDirection == LayoutDirection.RTL){
            return x <= mCrossAreaWidth;
        }
        return x >= getWidth() - mCrossAreaWidth;
    }

    private void drawImage(Canvas canvas){
        if (isEnableImage()) {
            PixelMap.InitializationOptions options = new PixelMap.InitializationOptions();
            options.size = new Size(Math.round(getHeight() - mBorderWidth), Math.round(getHeight() - mBorderWidth));
            PixelMap scaledImageBitmap = PixelMap.create(mBitmapImage, options);

            Paint paint = new Paint();
            paint.setAntiAlias(true);
            PixelMapHolder holder = new PixelMapHolder(scaledImageBitmap);
            paint.setShader(new PixelMapShader(holder, Shader.TileMode.CLAMP_TILEMODE, Shader.TileMode.CLAMP_TILEMODE), Paint.ShaderType.PIXELMAP_SHADER);
            RectFloat rect = new RectFloat(mBorderWidth, mBorderWidth, getHeight() - mBorderWidth, getHeight() - mBorderWidth);
            canvas.drawRoundRect(rect, rect.getHeight()/2, rect.getHeight()/2, paint);
        }
    }

    private void drawCross(Canvas canvas){
        if (isEnableCross()){
            mCrossAreaPadding = mCrossAreaPadding > getHeight() / 2 ? getHeight() / 2 :
                    mCrossAreaPadding;
            int ltX, ltY, rbX, rbY, lbX, lbY, rtX, rtY;
            ltX = mTextDirection == LayoutDirection.RTL ? (int)(mCrossAreaPadding) :
                    (int)(getWidth() - getHeight() + mCrossAreaPadding);
            ltY = mTextDirection == LayoutDirection.RTL ? (int)(mCrossAreaPadding) :
                    (int)(mCrossAreaPadding);
            lbX = mTextDirection == LayoutDirection.RTL ? (int)(mCrossAreaPadding) :
                    (int)(getWidth() - getHeight() + mCrossAreaPadding);
            lbY = mTextDirection == LayoutDirection.RTL ?
                    (int)(getHeight() - mCrossAreaPadding) : (int)(getHeight() - mCrossAreaPadding);
            rtX = mTextDirection == LayoutDirection.RTL ?
                    (int)(getHeight() - mCrossAreaPadding) : (int)(getWidth() - mCrossAreaPadding);
            rtY = mTextDirection == LayoutDirection.RTL ? (int)(mCrossAreaPadding) :
                    (int)(mCrossAreaPadding);
            rbX = mTextDirection == LayoutDirection.RTL ?
                    (int)(getHeight() - mCrossAreaPadding) : (int)(getWidth() - mCrossAreaPadding);
            rbY = mTextDirection == LayoutDirection.RTL ?
                    (int)(getHeight() - mCrossAreaPadding) : (int)(getHeight() - mCrossAreaPadding);

            mPaint.setStyle(Paint.Style.STROKE_STYLE);
            mPaint.setColor(new Color(mCrossColor));
            mPaint.setStrokeWidth(mCrossLineWidth);
            canvas.drawLine(ltX, ltY, rbX, rbY, mPaint);
            canvas.drawLine(lbX, lbY, rtX, rtY, mPaint);
        }
    }

    private void drawRipple(Canvas canvas) {

        canvas.save();
        mPath.reset();

        canvas.clipPath(mPath, Canvas.ClipOp.DIFFERENCE);
        mPath.addRoundRect(mRectF, mBorderRadius, mBorderRadius, Path.Direction.COUNTER_CLOCK_WISE);

        canvas.clipPath(mPath, Canvas.ClipOp.DIFFERENCE);

        canvas.drawCircle(mTouchX, mTouchY, mRippleRadius, mRipplePaint);

    }

    private void splashRipple() {
        mRipplePaint.setColor(new Color(mRippleColor));
        mRipplePaint.setAlpha(mRippleAlpha);
        final float maxDis = Math.max(Math.max(Math.max(mTouchX, mTouchY),
                Math.abs(getEstimatedWidth() - mTouchX)), Math.abs(getEstimatedHeight() - mTouchY));

        mRippleValueAnimator = new AnimatorValue();
        mRippleValueAnimator.setDuration(mRippleDuration);
        mRippleValueAnimator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                float animValue = maxDis * v;
                mRippleRadius = animValue >= maxDis ? 0 : animValue;
                invalidate();
            }
        });
        mRippleValueAnimator.start();
    }

    public String getText(){
        return mOriginText;
    }

    public boolean getIsViewClickable(){
        return isViewClickable;
    }

    public boolean getIsViewSelected(){
        return isViewSelected;
    }

    public void setTagMaxLength(int maxLength){
        this.mTagMaxLength = maxLength;
        onDealText();
    }

    public void setOnTagClickListener(OnTagClickListener listener){
        this.mOnTagClickListener = listener;
    }

    public int getTagBackgroundColor(){
        return mBackgroundColor;
    }

    public int getTagSelectedBackgroundColor(){
        return mSelectedBackgroundColor;
    }

    public void setTagBackgroundColor(int color){
        this.mBackgroundColor = color;
    }

    public void setTagSelectedBackgroundColor(int color){
        this.mSelectedBackgroundColor = color;
    }

    public void setTagBorderColor(int color){
        this.mBorderColor = color;
    }

    public void setTagTextColor(int color){
        this.mTextColor = color;
    }

    public void setBorderWidth(float width) {
        this.mBorderWidth = width;
    }

    public void setBorderRadius(float radius) {
        this.mBorderRadius = radius;
    }

    public void setTextSize(int size) {
        this.mTextSize = size;
        onDealText();
    }

    public void setHorizontalPadding(int padding) {
        this.mHorizontalPadding = padding;
    }

    public void setVerticalPadding(int padding) {
        this.mVerticalPadding = padding;
    }

    public void setIsViewClickable(boolean clickable) {
        this.isViewClickable = clickable;
    }

    public void setImage(PixelMap newImage) {
        this.mBitmapImage = newImage;
        this.invalidate();
    }

    public void setIsViewSelectable(boolean viewSelectable) {
        isViewSelectable = viewSelectable;
    }

    //TODO change background color
    public void selectView() {
        if (isViewSelectable && !getIsViewSelected()) {
            this.isViewSelected = true;
            invalidate();
        }
    }

    public void deselectView() {
        if (isViewSelectable && getIsViewSelected()) {
            this.isViewSelected = false;
            invalidate();
        }
    }

    public interface OnTagClickListener{
        void onTagClick(int position, String text);
        void onTagLongClick(int position, String text);
        void onSelectedTagDrag(int position, String text);
        void onTagCrossClick(int position);
    }

    public LayoutDirection getTextDirection() {
        return mTextDirection;
    }

    public void setTextDirection(LayoutDirection textDirection) {
        this.mTextDirection = textDirection;
    }

    public void setTypeface(Font typeface) {
        this.mTypeface = typeface;
        onDealText();
    }

    public void setRippleAlpha(int mRippleAlpha) {
        this.mRippleAlpha = mRippleAlpha;
    }

    public void setRippleColor(int mRippleColor) {
        this.mRippleColor = mRippleColor;
    }

    public void setRippleDuration(int mRippleDuration) {
        this.mRippleDuration = mRippleDuration;
    }

    public void setBdDistance(float bdDistance) {
        this.bdDistance = bdDistance;
    }

    public boolean isEnableImage() { return mBitmapImage != null && mTextDirection != LayoutDirection.RTL; }

    public boolean isEnableCross() {
        return mEnableCross;
    }

    public void setEnableCross(boolean mEnableCross) {
        this.mEnableCross = mEnableCross;
    }

    public float getCrossAreaWidth() {
        return mCrossAreaWidth;
    }

    public void setCrossAreaWidth(float mCrossAreaWidth) {
        this.mCrossAreaWidth = mCrossAreaWidth;
    }

    public float getCrossLineWidth() {
        return mCrossLineWidth;
    }

    public void setCrossLineWidth(float mCrossLineWidth) {
        this.mCrossLineWidth = mCrossLineWidth;
    }

    public float getCrossAreaPadding() {
        return mCrossAreaPadding;
    }

    public void setCrossAreaPadding(float mCrossAreaPadding) {
        this.mCrossAreaPadding = mCrossAreaPadding;
    }

    public int getCrossColor() {
        return mCrossColor;
    }

    public void setCrossColor(int mCrossColor) {
        this.mCrossColor = mCrossColor;
    }

    public boolean isTagSupportLettersRTL() {
        return mTagSupportLettersRTL;
    }

    public void setTagSupportLettersRTL(boolean mTagSupportLettersRTL) {
        this.mTagSupportLettersRTL = mTagSupportLettersRTL;
    }
}
