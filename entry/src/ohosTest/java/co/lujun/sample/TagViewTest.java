package co.lujun.sample;

import co.lujun.ohostagview.TagView;
import junit.framework.TestCase;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.Component;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.app.Context;

import java.lang.reflect.Field;

public class TagViewTest extends TestCase {

    public void testGetText() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        TagView tagView = new TagView(context, "test");
        assertEquals("test", tagView.getText());
    }

    public void testGetIsViewClickable() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        TagView tagView = new TagView(context, "test");
        tagView.setIsViewClickable(true);
        assertEquals(true, tagView.getIsViewClickable());
    }

    public void testGetIsViewSelected() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        TagView tagView = new TagView(context, "test");
        Field field = tagView.getClass().getDeclaredField("isViewSelected");
        field.setAccessible(true);
        field.set(tagView, true);
        assertEquals(true, tagView.getIsViewSelected());
    }

    public void testSetTagMaxLength() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        TagView tagView = new TagView(context, "test");
        tagView.setTagMaxLength(10);
        Field field = tagView.getClass().getDeclaredField("mTagMaxLength");
        field.setAccessible(true);
        assertEquals(10, field.get(tagView));
    }

    public void testSetOnTagClickListener() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        TagView tagView = new TagView(context, "test");
        tagView.setOnTagClickListener(new TagView.OnTagClickListener() {
            @Override
            public void onTagClick(int position, String text) {
            }

            @Override
            public void onTagLongClick(int position, String text) {
            }

            @Override
            public void onSelectedTagDrag(int position, String text) {
            }

            @Override
            public void onTagCrossClick(int position) {
            }
        });
        Field field = tagView.getClass().getDeclaredField("mOnTagClickListener");
        field.setAccessible(true);
        assertNotNull(field.get(tagView));
    }

    public void testGetTagBackgroundColor() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        TagView tagView = new TagView(context, "test");
        tagView.setTagBackgroundColor(Color.RED.getValue());
        assertEquals(Color.RED.getValue(), tagView.getTagBackgroundColor());
    }

    public void testGetTagSelectedBackgroundColor() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        TagView tagView = new TagView(context, "test");
        tagView.setTagSelectedBackgroundColor(Color.RED.getValue());
        assertEquals(Color.RED.getValue(), tagView.getTagSelectedBackgroundColor());
    }

    public void testSetTagBorderColor() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        TagView tagView = new TagView(context, "test");
        tagView.setTagBorderColor(Color.RED.getValue());
        Field field = tagView.getClass().getDeclaredField("mBorderColor");
        field.setAccessible(true);
        assertEquals(Color.RED.getValue(), field.get(tagView));
    }

    public void testSetTagTextColor() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        TagView tagView = new TagView(context, "test");
        tagView.setTagTextColor(Color.RED.getValue());
        Field field = tagView.getClass().getDeclaredField("mTextColor");
        field.setAccessible(true);
        assertEquals(Color.RED.getValue(), field.get(tagView));

    }

    public void testSetBorderWidth() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        TagView tagView = new TagView(context, "test");
        tagView.setBorderWidth(10f);
        Field field = tagView.getClass().getDeclaredField("mBorderWidth");
        field.setAccessible(true);
        assertEquals(10f, field.get(tagView));
    }

    public void testSetBorderRadius() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        TagView tagView = new TagView(context, "test");
        tagView.setBorderRadius(10f);
        Field field = tagView.getClass().getDeclaredField("mBorderRadius");
        field.setAccessible(true);
        assertEquals(10f, field.get(tagView));
    }

    public void testSetTextSize() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        TagView tagView = new TagView(context, null);
        tagView.setTextSize(10);
        Field field = tagView.getClass().getDeclaredField("mTextSize");
        field.setAccessible(true);
        assertEquals(10, field.get(tagView));
    }

    public void testSetHorizontalPadding() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        TagView tagView = new TagView(context, "test");
        tagView.setHorizontalPadding(10);
        Field field = tagView.getClass().getDeclaredField("mHorizontalPadding");
        field.setAccessible(true);
        assertEquals(10, field.get(tagView));
    }

    public void testSetVerticalPadding() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        TagView tagView = new TagView(context, "test");
        tagView.setVerticalPadding(10);
        Field field = tagView.getClass().getDeclaredField("mVerticalPadding");
        field.setAccessible(true);
        assertEquals(10, field.get(tagView));
    }

    public void testSelectView() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        TagView tagView = new TagView(context, "test");
        tagView.setIsViewSelectable(true);
        Field field = tagView.getClass().getDeclaredField("isViewSelected");
        field.setAccessible(true);
        field.set(tagView, false);
        tagView.selectView();
        assertEquals(true, field.get(tagView));
    }

    public void testDeselectView() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        TagView tagView = new TagView(context, "test");
        tagView.setIsViewSelectable(true);
        Field field = tagView.getClass().getDeclaredField("isViewSelected");
        field.setAccessible(true);
        field.set(tagView, true);
        tagView.deselectView();
        assertEquals(false, field.get(tagView));
    }

    public void testGetTextDirection() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        TagView tagView = new TagView(context, "test");
        tagView.setTextDirection(Component.LayoutDirection.LTR);
        assertEquals(Component.LayoutDirection.LTR, tagView.getTextDirection());
    }

    public void testSetTypeface() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        TagView tagView = new TagView(context, null);
        tagView.setTypeface(Font.DEFAULT_BOLD);
        Field field = tagView.getClass().getDeclaredField("mTypeface");
        field.setAccessible(true);
        assertEquals(Font.DEFAULT_BOLD, field.get(tagView));
    }

    public void testSetRippleAlpha() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        TagView tagView = new TagView(context, null);
        tagView.setRippleAlpha(1);
        Field field = tagView.getClass().getDeclaredField("mRippleAlpha");
        field.setAccessible(true);
        assertEquals(1, field.get(tagView));
    }

    public void testSetRippleColor() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        TagView tagView = new TagView(context, null);
        tagView.setRippleColor(Color.BLUE.getValue());
        Field field = tagView.getClass().getDeclaredField("mRippleColor");
        field.setAccessible(true);
        assertEquals(Color.BLUE.getValue(), field.get(tagView));
    }

    public void testSetRippleDuration() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        TagView tagView = new TagView(context, null);
        tagView.setRippleDuration(200);
        Field field = tagView.getClass().getDeclaredField("mRippleDuration");
        field.setAccessible(true);
        assertEquals(200, field.get(tagView));
    }

    public void testSetBdDistance() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        TagView tagView = new TagView(context, null);
        tagView.setBdDistance(20f);
        Field field = tagView.getClass().getDeclaredField("bdDistance");
        field.setAccessible(true);
        assertEquals(20f, field.get(tagView));
    }

    public void testIsEnableCross() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        TagView tagView = new TagView(context, null);
        tagView.setEnableCross(true);
        assertEquals(true, tagView.isEnableCross());
    }

    public void testGetCrossAreaWidth() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        TagView tagView = new TagView(context, null);
        tagView.setCrossAreaWidth(10f);
        assertEquals(10f, tagView.getCrossAreaWidth());
    }

    public void testGetCrossLineWidth() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        TagView tagView = new TagView(context, null);
        tagView.setCrossLineWidth(10f);
        assertEquals(10f, tagView.getCrossLineWidth());
    }

    public void testGetCrossAreaPadding() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        TagView tagView = new TagView(context, null);
        tagView.setCrossAreaPadding(10f);
        assertEquals(10f, tagView.getCrossAreaPadding());
    }

    public void testGetCrossColor() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        TagView tagView = new TagView(context, null);
        tagView.setCrossColor(Color.RED.getValue());
        assertEquals(Color.RED.getValue(), tagView.getCrossColor());
    }

    public void testIsTagSupportLettersRTL() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        TagView tagView = new TagView(context, null);
        tagView.setTagSupportLettersRTL(true);
        assertEquals(true, tagView.isTagSupportLettersRTL());
    }
}